"""empty message

Revision ID: 30ded57ec486
Revises: a7bf4af6a2d3
Create Date: 2020-07-13 20:21:30.534283

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '30ded57ec486'
down_revision = 'a7bf4af6a2d3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('resume', sa.Column('wpath', sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('resume', 'wpath')
    # ### end Alembic commands ###
